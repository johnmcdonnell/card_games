//******************************************
//*            John McDonnell              *
//*            K18Comp2                    *
//*       Games Application                *
//*     Programming Principles             *
//******************************************


//Import Utilities

//import java.util.Scanner;

// Declare Class
public class GamesApp
{
    
    //Main Method
    public static void main(String[] args)
    {
        int choice = displayMenu(); 
        while (choice != 3)
        {           
            switch(choice)
            {
                case 1:  
                    displayCardMenu();
                    System.out.println("Card Menu");
                    break;
                case 2:
                    displayGuessMenu(); 
                    System.out.println("Guess Menu");
                    break;
                case 3:
                    
                    break;        
            } 
            
            choice = displayMenu(); 
        } 
        System.out.println("Thank You Please Come Again Soon!!");
        
    }
    //Menu Method
    public static int displayMenu()
    {
        
        System.out.println("*********************************************************");
        System.out.println("\t\tMain Games Menu");
        System.out.println("*********************************************************");
        System.out.println("Please select the type of game you would like to play:");
        System.out.println("=========================================================\n");
        System.out.println("Please press [1] to select a Card Game.\n");   
        System.out.println("Please press [2] to select a Guessing Game.\n");
        System.out.println("Please press [3] to Exit this Games Menu.\n");
        int cat = Utilitys.validation(1, 3, "Please Select:", "Error - integer value not in correct range");
        
        return cat;
    }
    
    //Card Menu Method
    public static int displayCardMenu()
    {
        System.out.println("*********************************************************");
        System.out.println("\t\tCard Games Menu");
        System.out.println("*********************************************************");
        System.out.println("Please select which Card Game you would like to play:");
        System.out.println("=========================================================\n");
        System.out.println("Please press [1] to select to play Higher or Lower.\n");   
        System.out.println("Please press [2] to select Black Jack Betting.\n");
        System.out.println("Please press [3] to select Exit back to Main Menu.\n");
        int type = Utilitys.validation( 1,3, "Please Select:", "Error - integer value not in correct range"); 
        
        while (type != 3)
        {           
            switch(type)
            {
                case 1:  
                    Games.playHigherLower();
                    
                    break;
                case 2:
                     Games.playBlackJackBetting();
                    
                    break;    
                case 3:
                    displayMenu();
            } 
            type = displayCardMenu(); 
            
        }
        return type;
    }
    
    //Guess Menu Method
    public static int displayGuessMenu()
    {
        System.out.println("*********************************************************");
        System.out.println("\t\tGuessing Games Menu");
        System.out.println("*********************************************************");
        System.out.println("Please select which Guessing Game you would like to play:");
        System.out.println("=========================================================\n");
        System.out.println("Please press [1] to select to play a Random Number Game.\n");   
        System.out.println("Please press [2] to select to play a Random Day Game.\n");
        System.out.println("Please press [3] to select Exit back to Main Menu.\n");
        int guess = Utilitys.validation(1, 3, "Please Select:", "Error - integer value not in correct range"); 
        while (guess != 3)
        {           
            switch(guess)
            {
                case 1:  
                     Games.playRandomNumber();
                    
                    break;
                case 2:
                     Games.playRandomDay(); 
                    
                    break;
            } 
            guess = displayGuessMenu(); 
            
        }
        
        return guess;
    }
}
    

