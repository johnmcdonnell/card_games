//******************************************
//*          John McDonnell                *
//*            K18Comp2                    *
//*       Games Application                *
//*     Programming Principles             *
//******************************************



//<---------------------------------------------------------------------------------->//
//<--------------------------------Games-------------------------------------------->//
//<-------------------------------------------------------------------------------->//   

//<----------------------------Random Number--------------------------------------->//

import java.util.Random;
import java.util.Scanner;

public class Games
{     
    
    
    public static void playRandomNumber()
    {
        System.out.println("**********************************************************");
        System.out.println("You have chosen to play a Random Number Game");
        System.out.println("**********************************************************\n");
        Random rand = new Random();
        
        int count = 0;
        int i = 0;
        
        System.out.println("*********************** Game RULES ***********************");
        System.out.println("Can you guess the number between 1 & 10 ? ");
        System.out.println("You only have 5 attempts. Get as many correct as you can !!!");
        System.out.println("*********************** GOOD LUCK ************************\n");
        int numberToGuess = rand.nextInt(10);
        while(count != 5 && i == 0)
        {
            System.out.println("Guess a number between 1 and 10: ");
            
            int guess = Utilitys.validation(1, 10, "Please Select:", "Error - integer value not in correct range"); 
            if (guess == numberToGuess)
            {
                System.out.println("Congratulations your guess was correct you win: \n");
                i= 1;
            }
            else if ( guess < numberToGuess)
            {
                System.out.println("Your guess is too low!!! ");
            }
            else if ( guess > numberToGuess)
            {
                System.out.println("Your guess is too high!!! ");
            }
            count ++;
        }
        
        System.out.println("Returning to Game Menu.......\n\n");
    }
    
//<---------------------------Random Day----------------------------------------->//    
    
    public static void playRandomDay()
    {
        
        Scanner keyboard = new Scanner(System.in);
        System.out.println("***********************************************************");
        System.out.println("You have chosen to play a Random Day Game");
        System.out.println("*********************** Game RULES ************************");
        System.out.println("Can you guess what day a day of the week? ");
        System.out.println("You only have two trys. Get as many correct as you can !!!");
        System.out.println("*********************** GOOD LUCK *************************\n");
        System.out.println("*  Monday  \t= 1 *");
        System.out.println("*  Tuesday \t= 2 *");
        System.out.println("*  Wednesday \t= 3 *");
        System.out.println("*  Thursday \t= 4 *");
        System.out.println("*  Friday  \t= 5 *");
        System.out.println("*  Saturday \t= 6 *");
        System.out.println("*  Sunday  \t= 7 *");
        System.out.println("*********************************");
        int guessOne = Utilitys.validation(1, 7, "Please Select:", "Error - integer value not in correct range"); 
        System.out.println("Guess a number between 1 and 7: ");
        guessOne = keyboard.nextInt(); 
        
        Random rand = new Random();
        int day =  rand.nextInt(7) + 1;
        
        String dayName = "";
        
        if (day==1)
        {
            dayName = "Monday";
        }        
        else if (day==2)
        {
            dayName = "Tuesday";
        }
        else if (day==3)
        {
            dayName = "Wednesday";
        }
        else if (day==4)
        {
            dayName = "Thursday";
        }
        else if (day==5)
        {
            dayName = "Friday";
        }
        else if (day==6)
        {
            dayName = "Saturday";          
        }
        else if (day==7)
        {
            dayName = "Sunday";  
        }
        if (guessOne == day)
        {
            System.out.println("Congratulations you have chosen the correct day ");
        }
        else
        {
            System.out.println("You Lose");
        }
        System.out.println("You're guess was : " + guessOne);
        System.out.println("The answer was : " + dayName);
        
        System.out.println("Guess a number between 1 and 7: ");
        guessOne = keyboard.nextInt(); 
    }
    
//<---------------------------Random Name----------------------------------------->//    
    
    public static void playRandomName()
    {
        System.out.println("**********************************");
        System.out.println("You have Chosen Random Name Game");
        System.out.println("**********************************\n");
    }
    
//<---------------------------Higher/Lower---------------------------------------->//    
    
    public static void playHigherLower()
    {
        System.out.println("**********************************");
        System.out.println("You have Chosen Higher or Lower");
        System.out.println("**********************************\n");
    }
//<---------------------------Black Jack------------------------------------------>//    
    public static void playBlackJack()
    {
        System.out.println("**********************************");
        System.out.println("You have Chosen Black Jack");
        System.out.println("**********************************\n");
    }
    
//<---------------------------Black Jack Betting--------------------------------->//    
    
    
    public static void playBlackJackBetting()
    {
        Scanner keyboard = new Scanner(System.in);
        int playerCardNumberNew = 0;
        int dealerChoice = 0;
        int userChoice = 0;
        
        System.out.println("*****************************************");
        System.out.println("You have chosen to play BlackJack Betting");
        System.out.println("***************************************\n");
        
        
//        System.out.println("What is your name?");
//        String playerOne = keyboard.nextLine();
        //Get  Players Cards
        
        int playerCardNumberOne = Utilitys.getUniqueRandomNumber(1,52);
        String playerCardNameOne = Utilitys.getCardName(playerCardNumberOne);
        int playerCardNumberTwo = Utilitys.getUniqueRandomNumber(1,52);
        String playerCardNameTwo = Utilitys.getCardName(playerCardNumberTwo);
        playerCardNumberOne = playerCardNumberOne %13;
        playerCardNumberTwo = playerCardNumberTwo %13;
        
        playerCardNumberOne = Utilitys.getCardValueBlackJack(playerCardNumberOne);
        playerCardNumberTwo = Utilitys.getCardValueBlackJack(playerCardNumberTwo);
        
        int playerTotal = playerCardNumberOne + playerCardNumberTwo;
        
//Get  dealers Cards
        
        int dealerCardNumberOne = Utilitys.getUniqueRandomNumber(1,52);
        String dealerCardNameOne = Utilitys.getCardName(dealerCardNumberOne);
        int dealerCardNumberTwo = Utilitys.getUniqueRandomNumber(1,52);
        String dealerCardNameTwo = Utilitys.getCardName(dealerCardNumberTwo);
        dealerCardNumberOne = dealerCardNumberOne %13;
        dealerCardNumberTwo = dealerCardNumberTwo %13;
        
        dealerCardNumberOne = Utilitys.getCardValueBlackJack(dealerCardNumberOne);
        dealerCardNumberTwo = Utilitys.getCardValueBlackJack(dealerCardNumberTwo);
        int dealerTotal = dealerCardNumberOne + dealerCardNumberTwo;
        System.out.println("You Did Not Get BlackJack! ");
        System.out.println("Your cards are " + playerCardNumberOne + playerCardNameOne  + " & " + playerCardNumberTwo + playerCardNameTwo +" = " + playerTotal +" \nTo Continue Press (1) To Hit Or (2) To Hold\n");
        userChoice = Utilitys.validation(1, 2, "Please Select:", "Error - integer value not in correct range");
        int i =0;
        while ( i == 0 )
        {
            
            
            if (userChoice == 1 && playerTotal < 21 )
            {
                System.out.println("You've Decided to hit" );
                System.out.println("New Card Dealt");
                playerCardNumberNew = Utilitys.getUniqueRandomNumber(1,52);
                String playerCardNameNew = Utilitys.getCardName(playerCardNumberNew);
                playerCardNumberNew = Utilitys.getCardValueBlackJack(playerCardNumberNew);
                playerTotal += playerCardNumberNew;
                System.out.println("New Card = " + playerCardNameNew );
                
                
                
                
                if ( playerTotal < 21 )
                {
                    System.out.println("You Did Not Get BlackJack! ");
                    System.out.println("Your cards are " + playerCardNumberOne + playerCardNameOne  + " & " + playerCardNumberTwo + playerCardNameTwo  + " = " + playerTotal +" \nTo Continue Press (1) To Hit Or (2) To Hold\n");
                    System.out.println( playerCardNumberNew );
                    userChoice = Utilitys.validation(1, 2, "Please Select:", "Error - integer value not in correct range");
                }
                else 
                {
                    i = 0;
                    System.out.println("Bust ");
                }
                
                
            }
            else if ( userChoice == 2 )
            {
                
                System.out.println("You've Decided to stand" );
                System.out.println("Your cards are " + playerCardNameOne + " & " + playerCardNameTwo  + " = " + playerTotal );
                i=1;
            }
            
            
            if (dealerTotal < 17 && dealerTotal != 21 )
            {
                System.out.println("Dealer has hit Also" );
                System.out.println("New Card Dealt");
                int dealerCardNumberNew = Utilitys.getUniqueRandomNumber(1,52);
                String dealerCardNameNew = Utilitys.getCardName(dealerCardNumberNew);
                dealerTotal += dealerCardNumberNew;
                dealerCardNumberNew = Utilitys.getCardValueBlackJack(dealerCardNumberNew);
            }
            else if (dealerTotal >= 17 && dealerTotal <= 21)
            {
                dealerChoice = 2;
                System.out.println("Dealer has decided to stick" );
//                dealerTotal = dealerTotal;
                
            }
            
        }
        System.out.println("Dealers total = " + dealerTotal );
        System.out.println("Players total = " + playerTotal );
        if ( playerTotal > 21 )
        {
            System.out.println("You have bust, The dealer wins!!" );
        }
        else if ( dealerTotal > 21)
        {
            System.out.println("Dealer has bust, Congratulations you have won!!" );
        }
        else if ((21 - playerTotal) == (21 - dealerTotal))
        {
            System.out.println("Its a draw!!" );
        }
        System.out.println("Returning to Game Menu.......\n\n");
    }
    
}
