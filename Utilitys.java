//******************************************
//*           John McDonnell               *
//*            K18Comp2                    *
//*       Games Application                *
//*     Programming Principles             *
//******************************************


import java.util.Scanner;
import java.util.Random; 

public class Utilitys
{     
    
    public static int validation(int min, int max, String prompt, String errorPrompt)
    {
        Scanner keyboard = new Scanner(System.in);
        int value;
        
        while(!keyboard.hasNextInt())
        {             
            System.out.print("Not an integer value - please re-enter.");
            keyboard.nextLine();                                        
        }  
        value = keyboard.nextInt(); 
        
        boolean notInRange = value < min || value > max;
        while (notInRange)
        {         
            System.out.println(errorPrompt + "[" + min + " : " + max +"]");  
            keyboard.nextLine();   
            System.out.print(prompt);  
            
            value = keyboard.nextInt(); 
            
            notInRange = value < min || value > max;
        }          
        return value; 
        
    }
    
    private static int[] cardArray = null;
    
    public static int getUniqueRandomNumber(int lo, int hi)
    {
        if (cardArray == null)
        {
            startNewGame();
        }
        
        
        
        int number = Utilitys.getRandomInt(lo, hi);
        while(cardArray[number] == 1)
        { 
            number = Utilitys.getRandomInt(lo, hi);
            
        }
        
        //now set the cardArray value at this index to 1 to indicate the number is taken
        cardArray[number] = 1;
        cardCount++;
        
        return number;
    }
    
    public static int cardCount = 0;
    
    public static void startNewGame()
    {
        
        cardArray = new int[53];
        
        
        cardCount = 0;
        
        
        initialiseCardArray();
    }
    
    
    private static void initialiseCardArray()
    {
        for (int i = 0; i < cardArray.length; i++)
        {
            cardArray[i] = 0;
        }
    }
    public static int getRandomInt(int low, int high)
    {
        Random rand = new Random();
        int num;
        num = rand.nextInt(high - low + 1) + low;       
        return num;
    } 
    
    public static String getCardName(int card)
    {
        
        
        String suit ="";
        String face ="";
        
        
        if ( card >= 1 && card <= 13)
        {
            suit = "Spade";    
            
        }
        else if ( card > 13 && card <= 26) 
        {
            suit = "Clubs";
            
        }  
        else if ( card > 26 && card <= 39)
        {
            suit = "Diamonds";
            
        }
        else if  ( card > 39 )
        {
            suit = "Hearts";
            
        } 
        
        if ( card % 13 + 1 == 1)
        {
            face = "Ace";
        }
        else if ( card % 13 + 1 == 2)
        {
            face = "Two";
        }
        else if ( card % 13 + 1 == 3)
        {
            face = "Three";
        }
        else if ( card % 13 + 1 == 4)
        {
            face = "Four";
        }
        else if ( card % 13 + 1 == 5)
        {
            face = "Five";
        }
        else if ( card % 13 + 1 == 6)
        {
            face = "Six";
        }
        else if ( card % 13 + 1 == 7)
        {
            face = "Seven";
        }
        else if ( card % 13 + 1 == 8)
        {
            face = "Eight";
        }
        else if ( card % 13 + 1 == 9)
        {
            face = "Nine";
        }
        else if ( card % 13 + 1 == 10)
        {
            face = "Ten";
        }
        else if ( card % 13 + 1 == 11)
        {
            face = "Jack";
        }
        else if ( card % 13 + 1 == 12)
        {
            face = "Queen";
        }
        else if ( card % 13 + 1 == 13)
        {
            face = "King";
        }
        
        
        String userCard = (face + " of " + suit);
        return userCard;
    }
    
    
    public static int getCardValueBlackJack(int card)
    {
        int number = card % 13 + 1 ;
        int ans = 0;
        
        if ( number == 1)
        {
            ans = 1;
        }
        
        else if ( number == 11)
        {
            ans = 10;
        }
        else if ( number == 12)
        {
            ans = 10;
        }
        else if ( number == 13)
        {
            ans = 10;
        }
        else
        {            
            ans = number;
        }
        return ans;
        
    }
    
}